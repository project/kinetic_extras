<?php

namespace Drupal\kinetic_extras\Drush\Generators;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use DrupalCodeGenerator\Command\BaseGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use DrupalCodeGenerator\Application;
use DrupalCodeGenerator\Asset\AssetCollection;
use DrupalCodeGenerator\Asset\File;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\GeneratorType;
use DrupalCodeGenerator\InputOutput\Interviewer;
use DrupalCodeGenerator\Utils;
use DrupalCodeGenerator\Validator\Choice;
use DrupalCodeGenerator\Validator\Optional;
use DrupalCodeGenerator\Validator\Required;
use DrupalCodeGenerator\Validator\RequiredMachineName;
use Symfony\Component\Console\Question\Question;

#[Generator(
  name: 'kinetic:sdc',
  description: 'Generates Drupal SDC theme component for Kinetic theme',
  aliases: ['kc'],
  templatePath: __DIR__,
  type: GeneratorType::THEME_COMPONENT,
)]
final class ComponentGenerator extends BaseGenerator implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly ThemeHandlerInterface $themeHandler,
    private readonly LibraryDiscoveryInterface $libraryDiscovery,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('module_handler'),
      $container->get('theme_handler'),
      $container->get('library.discovery'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars, AssetCollection $assets): void {
    $this->askQuestions($vars);
    $this->generateAssets($vars, $assets);
  }

  /**
   * Collects user answers.
   */
  private function askQuestions(array &$vars): void {
    $ir = $this->createInterviewer($vars);
    $vars['machine_name'] = 'kinetic';
    $vars['name'] = 'Kinetic';
    $this->setUpDestination($vars);

    $vars['component_name'] = $ir->ask('Component name', validator: new Required());
    $vars['component_machine_name'] = $ir->ask(
      'Component machine name',
      Utils::human2machine($vars['component_name']),
      new RequiredMachineName(),
    );

    $vars['component_description'] = $ir->ask('Component description (optional)');

    $this->selectEntityTypeHookup($vars);
    $this->handleEntityTypeTemplateName($vars);

    $vars['component_libraries'] = [];
    do {
      $library = $this->askLibrary();
      $vars['component_libraries'][] = $library;
    } while ($library !== NULL);

    $vars['component_libraries'] = \array_filter($vars['component_libraries']);
    $vars['component_has_css'] = $ir->confirm('Needs CSS?');
    $vars['component_has_js'] = $ir->confirm('Needs JS?');
    if ($ir->confirm('Needs component props?')) {
      $vars['component_props'] = [];
      do {
        $prop = $this->askProp($vars, $ir);
        $vars['component_props'][] = $prop;
      } while ($ir->confirm('Add another prop?'));
    }
    $vars['component_props'] = \array_filter($vars['component_props'] ?? []);
  }

  /**
   * Sets up the destination directory.
   *
   * @param array $vars
   *   The answers to the CLI questions.
   */
  private function setUpDestination(&$vars) {
    $destination_choices = [
      'source/02-components/00-elements' => 'Elements',
      'source/02-components/01-composites' => 'Composites',
      'source/02-components/02-blocks' => 'Blocks',
      'source/02-components/03-formations' => 'Formations',
      'source/02-components/04-pages' => 'Pages',
    ];
    $choice = $this->io()
      ->choice('Choose destination', array_values($destination_choices), 'Blocks');
    // get the key of the choice
    $vars['directory'] = \array_search($choice, $destination_choices);
  }

  private function selectEntityTypeHookup(&$vars) {
    $choices = [
      'none' => 'None',
      'block' => 'Block',
      'paragraph' => 'Paragraph',
      //      'taxonomy' => 'Taxonomy',
      //      'media' => 'Media',
      //      'node' => 'Node',
    ];
    $choice = $this->io()->choice('Stub a entity template?', array_values($choices), 'none');
    $vars['entityType'] = \array_search($choice, $choices);
  }

  // handle the entity type template naming if entity type is selected.
  private function handleEntityTypeTemplateName(&$vars) {
    if ($vars['entityType']) {
      if ($vars['entity_machine_name']) {
        $name_suggestion = $vars['entity_machine_name'];
      }
      else {
        $name_suggestion = $vars['component_machine_name'];
      }
      $name_suggestion = str_replace('_', '-', $name_suggestion);
      $full_name_suggestion = $vars['entityType'] . '--' . $name_suggestion;
      $vars['entity_template_name'] = $this->io()->ask('Entity template name (will add .html.twig for you)', $full_name_suggestion);
    }
  }



  /**
   * Create the assets that the framework will write to disk later on.
   *
   * @psalm-param array{component_has_css: bool, component_has_js: bool} $vars
   *   The answers to the CLI questions.
   */
  private function generateAssets(array $vars, AssetCollection $assets): void {
    $component_path = '{directory}' . DIRECTORY_SEPARATOR . '{component_machine_name}' . DIRECTORY_SEPARATOR;

    if ($vars['component_has_css']) {
      $assets->addFile($component_path . '{component_machine_name}.scss', 'main-css--template.twig');
    }
    if ($vars['component_has_js']) {
      $assets->addFile($component_path . '{component_machine_name}.es6.js', 'main-js--template.twig');
    }

    $assets->addFile($component_path . '{component_machine_name}.twig', 'component-twig--template.twig');
    $assets->addFile($component_path . '{component_machine_name}.component.yml', 'component-yml--template.twig');
    $assets->addFile($component_path . 'README.md', 'readme-md--template.twig');

    $contents = \file_get_contents($this->getTemplatePath() . \DIRECTORY_SEPARATOR . 'thumbnail.jpg');
    $thumbnail = new File($component_path . 'thumbnail.jpg');
    $thumbnail->content($contents);
    $assets[] = $thumbnail;

    $entity_path_token = $this->createEntityPathToken($vars);
    $this->createEntityTemplate($vars, $entity_path_token, $assets);
  }

  // handle entity template generation.
  private function createEntityPathToken(array $vars): string {
    $entity_path_token = '';
    if ($vars['entityType']) {
      if ($vars['entityType'] === 'block') {
        $entity_path_token = 'templates/' . $vars['entityType'] . '/' . '{component_machine_name}' . DIRECTORY_SEPARATOR;
      }
      else {
        // for other entity types (paragraph, taxonomy, media, node
        $entity_path_token = 'templates/' . $vars['entityType'] . DIRECTORY_SEPARATOR;
      }
    }
    return $entity_path_token;
  }

  // handle entity template generation.
  private function createEntityTemplate(array $vars, $entity_path_token, AssetCollection $assets): void {
    if ($vars['entityType']) {
      // add a switch statement to handle the different entity types.
      switch ($vars['entityType']) {
        case 'block':
          $assets->addFile($entity_path_token . $vars['entity_template_name'] . '.html.twig', 'entity--block-html-twig--template.twig');
          break;
        case 'paragraph':
          $assets->addFile($entity_path_token . $vars['entity_template_name'] . '.html.twig', 'entity--paragraph-html-twig--template.twig');
          break;
      }
    }
  }



  /**
   * Prompts the user for a library.
   *
   * This helper gathers all the libraries from the system to allow autocomplete
   * and validation.
   *
   * @return string|null
   *   The library ID, if any.
   *
   * @todo Move this to interviewer.
   */
  private function askLibrary(): ?string {
    $extensions = [
      'core',
      ...\array_keys($this->moduleHandler->getModuleList()),
      ...\array_keys($this->themeHandler->listInfo()),
    ];
    $library_ids = \array_reduce(
      $extensions,
      fn (iterable $libs, $extension): array => \array_merge(
        (array) $libs,
        \array_map(static fn (string $lib): string => \sprintf('%s/%s', $extension, $lib),
          \array_keys($this->libraryDiscovery->getLibrariesByExtension($extension))),
      ),
      [],
    );

    $question = new Question('Library dependencies (optional). [Example: core/once]');
    $question->setAutocompleterValues($library_ids);
    $question->setValidator(
      new Optional(new Choice($library_ids, 'Invalid library selected.')),
    );

    return $this->io()->askQuestion($question);
  }

  /**
   * Asks for multiple questions to define a prop and its schema.
   *
   * @psalm-param array{component_machine_name: mixed, ...<array-key, mixed>} $vars
   *   The answers to the CLI questions.
   *
   * @return array
   *   The prop data, if any.
   */
  protected function askProp(array $vars, Interviewer $ir): array {
    $prop = [];
    $prop['title'] = $ir->ask('Prop title', '', new Required());
    $default = Utils::human2machine($prop['title']);
    $prop['name'] = $ir->ask('Prop machine name', $default, new RequiredMachineName());
    $prop['description'] = $ir->ask('Prop description (optional)');
    $choices = [
      'string' => 'String',
      'number' => 'Number',
      'boolean' => 'Boolean',
      'array' => 'Array',
      'object' => 'Object',
      'null' => 'Always null',
    ];
    $prop['type'] = $ir->choice('Prop type', $choices, 'String');
    if (!\in_array($prop['type'], ['string', 'number', 'boolean'])) {
      /** @psalm-var string $type */
      $type = $prop['type'];
      $component_schema_name = $vars['component_machine_name'] . '.component.yml';
      $this->io()->warning(\sprintf('Unable to generate full schema for %s. Please edit %s after generation.', $type, $component_schema_name));
    }
    return $prop;
  }

}
