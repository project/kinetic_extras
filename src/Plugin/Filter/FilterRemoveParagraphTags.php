<?php

namespace Drupal\kinetic_extras\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_remove_p",
 *   title = @Translation("Remove p tags"),
 *   description = @Translation("Remove p tags. Place after Convert line breaks into HTML filter if on."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class FilterRemoveParagraphTags extends FilterBase {

  public function process($text, $langcode) {
    // This will remove all p tags unless there's a class on it.
    $search = ['<p>', '</p>'];
    $replace = [''];
    $new_text = str_replace($search, $replace, $text);
    return new FilterProcessResult($new_text);
  }

}
